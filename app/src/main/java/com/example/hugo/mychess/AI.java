package com.example.hugo.mychess;

import java.util.ArrayList;
import java.util.Collections;

public class AI {


    LegalMoves lm = new LegalMoves();

    int wP1Loc = 8;    //white pawn x location
    int wP2Loc = 9;
    int wP3Loc = 10;
    int wP4Loc = 11;
    int wP5Loc = 12;
    int wP6Loc = 13;
    int wP7Loc = 14;
    int wP8Loc = 15;
    int wKLLoc = 1;    //white knight left
    int wKRLoc = 6;    //white knight right
    int wBLLoc = 2;    //white bishop left
    int wBRLoc = 5;    //white bishop right
    int wRLLoc = 0;    //white rook left
    int wRRLoc = 7;    //white rook right
    int wQLoc = 3;      //white queen
    int wKLoc = 4;      //white king

    int bP1Loc = 55;    //black pawn x location
    int bP2Loc = 54;
    int bP3Loc = 53;
    int bP4Loc = 52;
    int bP5Loc = 51;
    int bP6Loc = 50;
    int bP7Loc = 49;
    int bP8Loc = 48;
    int bKLLoc = 62;    //black knight left
    int bKRLoc = 57;    //black knight right
    int bBLLoc = 61;    //black bishop left
    int bBRLoc = 58;    //black bishop right
    int bRLLoc = 63;    //black rook left
    int bRRLoc = 56;    //black rook right
    int bQLoc = 59;      //black queen
    int bKLoc = 60;      //black king


    String TAG = "";

    int[] theBoard;

    ArrayList<Integer> temp1 = new ArrayList<>();
    ArrayList<Integer> temp2 = new ArrayList<>();
    ArrayList<Integer> comp = new ArrayList<>();


    public ArrayList<Integer> Move(int pos, int piece) {    //int loc, String piece, ArrayList arr
        temp1.clear();
        temp2.clear();
        ArrayList<Integer> arr;

        switch (piece) {
            case 1:
                arr = lm.bRookMoves(pos, "brl");
                for (int i = 0; i < arr.size(); i++) {  // first move`
                    int y = arr.get(i);
                    temp1 = lm.bRookMoves(y, "brl");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array

                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }
                }
                return arr;

            case 2:
                arr = lm.bKnightMoves(pos, "bkl");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.bKnightMoves(y, "bkl");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array

                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }
                }
                return temp1;

            case 3:
                arr = lm.bBishopMoves(pos, "bbl");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.bBishopMoves(y, "bbl");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array
                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }
                }
                return temp1;


            case 4:
                arr = lm.kingMoves(pos, "bk");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.kingMoves(y, "bk");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array
                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }

                }
                return temp1;

            case 5:
                arr = lm.queenMoves(pos, "bq");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.queenMoves(y, "bq");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array
                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }

                }
                return temp1;

            case 6:
                arr = lm.bBishopMoves(pos, "bbr");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.bBishopMoves(y, "bbr");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array
                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }

                }
                return temp1;

            case 7:
                arr = lm.bRookMoves(pos, "bkr");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.bRookMoves(y, "bkr");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array
                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }

                }
                return temp1;

            case 8:
                arr = lm.bRookMoves(pos, "brr");
                for (int i = 0; i < arr.size(); i++) {  // first move
                    int y = arr.get(i);
                    temp1 = lm.bRookMoves(y, "brr");
                    Collections.sort(temp1);
                    temp2.add(temp1.get(i));//adds lowest position to second array
                    for (int j = 0; j < temp2.size(); j++) {
                        if (temp2.get(j) > temp2.get(j + 1)) {
                            temp1.remove(j);
                        }
                    }

                }
                return temp1;
        }

        return null;
    }

}



