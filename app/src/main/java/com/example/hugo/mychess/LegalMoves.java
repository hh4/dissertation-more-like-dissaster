package com.example.hugo.mychess;

import android.util.Log;

import java.util.ArrayList;


public class LegalMoves {


//    MainActivity mainActivity = new MainActivity();

//    AI ai = new AI();


    //    legal moves for left and right, white knights
    ArrayList<Integer> wKL = new ArrayList<>();
    ArrayList<Integer> wKR = new ArrayList<>();
    ArrayList<Integer> wBL = new ArrayList<>();
    ArrayList<Integer> wBR = new ArrayList<>();
    ArrayList<Integer> wRL = new ArrayList<>();
    ArrayList<Integer> wRR = new ArrayList<>();
    ArrayList<Integer> wK = new ArrayList<>();
    ArrayList<Integer> wQ = new ArrayList<>();

    ArrayList<Integer> bKL = new ArrayList<>();
    ArrayList<Integer> bKR = new ArrayList<>();
    ArrayList<Integer> bBL = new ArrayList<>();
    ArrayList<Integer> bBR = new ArrayList<>();
    ArrayList<Integer> bRL = new ArrayList<>();
    ArrayList<Integer> bRR = new ArrayList<>();
    ArrayList<Integer> bK = new ArrayList<>();
    ArrayList<Integer> bQ = new ArrayList<>();

    ArrayList<Integer> p = new ArrayList<>();


//    ArrayList<Integer> check = new ArrayList<>();


//    ArrayList<Enum> myBoard = new ArrayList<>(64);


//    ArrayList<Enum> myBoard = new ArrayList<>();

    int[] theBoard = new int[64];

    Boolean wCheck = false;
    Boolean bCheck = false;


    String TAG = "";

    //    respective methods
    private char theFile;
    private int theRank;

    protected void setPieces() {

//        white pieces
        theBoard[0] = 5;    //left rook
        theBoard[1] = 3;    //left knight
        theBoard[2] = 3;    //left bishop
        theBoard[3] = 9;    //queen
        theBoard[4] = 999;    //king
        theBoard[5] = 3;    //right bishop
        theBoard[6] = 3;    //right knight
        theBoard[7] = 5;    //right rook
        theBoard[8] = 1;    //pawn1
        theBoard[9] = 1;    //pawn2
        theBoard[10] = 1;   //pawn3
        theBoard[11] = 1;   //pawn4
        theBoard[12] = 1;   //pawn5
        theBoard[13] = 1;   //pawn6
        theBoard[14] = 1;   //pawn7
        theBoard[15] = 1;   //pawn8

//        black pieces
        theBoard[63] = -5;    //left rook
        theBoard[62] = -3;    //left knight
        theBoard[61] = -3;    //left bishop
        theBoard[59] = -9;    //queen
        theBoard[60] = -999;    //king
        theBoard[58] = -3;    //right bishop
        theBoard[57] = -3;    //right knight
        theBoard[56] = -5;    //right rook
        theBoard[55] = -1;    //pawn1
        theBoard[54] = -1;    //pawn2
        theBoard[53] = -1;   //pawn3
        theBoard[52] = -1;   //pawn4
        theBoard[51] = -1;   //pawn5
        theBoard[50] = -1;   //pawn6
        theBoard[49] = -1;   //pawn7
        theBoard[48] = -1;   //pawn8

        for (int i = 0; i < theBoard.length; i++) {

            if (theBoard[i] > 15 && theBoard[i] < 48) {
                theBoard[i] = 0;
            }

        }
    }

//    public void setBoard(){
//        ai.getBoard(theBoard);
//    }

    //    returns current rank
    protected int currentRank(int pos) {
        if (pos < 8) {
            theRank = 1;
            return theRank;
        } else if (pos < 16) {
            theRank = 2;
            return theRank;
        } else if (pos < 24) {
            theRank = 3;
            return theRank;
        } else if (pos < 32) {
            theRank = 4;
            return theRank;
        } else if (pos < 40) {
            theRank = 5;
            return theRank;
        } else if (pos < 48) {
            theRank = 6;
            return theRank;
        } else if (pos < 56) {
            theRank = 7;
            return theRank;
        } else if (pos < 64) {
            theRank = 8;
            return theRank;
        }
        return theRank;
    }

    //    returns current file
    protected char currentFile(int pos) {

        switch (pos) {
//            Rank 1
            case 0:
                theFile = 'a';
                return theFile;
            case 1:
                theFile = 'b';
                return theFile;
            case 2:
                theFile = 'c';
                return theFile;
            case 3:
                theFile = 'd';
                return theFile;
            case 4:
                theFile = 'e';
                return theFile;
            case 5:
                theFile = 'f';
                return theFile;
            case 6:
                theFile = 'g';
                return theFile;
            case 7:
                theFile = 'h';
                return theFile;

//            Rank 2
            case 8:
                theFile = 'a';
                return theFile;
            case 9:
                theFile = 'b';
                return theFile;
            case 10:
                theFile = 'c';
                return theFile;
            case 11:
                theFile = 'd';
                return theFile;
            case 12:
                theFile = 'e';
                return theFile;
            case 13:
                theFile = 'f';
                return theFile;
            case 14:
                theFile = 'g';
                return theFile;
            case 15:
                theFile = 'h';
                return theFile;

//            Rank 3
            case 16:
                theFile = 'a';
                return theFile;
            case 17:
                theFile = 'b';
                return theFile;
            case 18:
                theFile = 'c';
                return theFile;
            case 19:
                theFile = 'd';
                return theFile;
            case 20:
                theFile = 'e';
                return theFile;
            case 21:
                theFile = 'f';
                return theFile;
            case 22:
                theFile = 'g';
                return theFile;
            case 23:
                theFile = 'h';
                return theFile;

//            Rank 4
            case 24:
                theFile = 'a';
                return theFile;
            case 25:
                theFile = 'b';
                return theFile;
            case 26:
                theFile = 'c';
                return theFile;
            case 27:
                theFile = 'd';
                return theFile;
            case 28:
                theFile = 'e';
                return theFile;
            case 29:
                theFile = 'f';
                return theFile;
            case 30:
                theFile = 'g';
                return theFile;
            case 31:
                theFile = 'h';
                return theFile;

//            Rank 5
            case 32:
                theFile = 'a';
                return theFile;
            case 33:
                theFile = 'b';
                return theFile;
            case 34:
                theFile = 'c';
                return theFile;
            case 35:
                theFile = 'd';
                return theFile;
            case 36:
                theFile = 'e';
                return theFile;
            case 37:
                theFile = 'f';
                return theFile;
            case 38:
                theFile = 'g';
                return theFile;
            case 39:
                theFile = 'h';
                return theFile;

//            Rank 6
            case 40:
                theFile = 'a';
                return theFile;
            case 41:
                theFile = 'b';
                return theFile;
            case 42:
                theFile = 'c';
                return theFile;
            case 43:
                theFile = 'd';
                return theFile;
            case 44:
                theFile = 'e';
                return theFile;
            case 45:
                theFile = 'f';
                return theFile;
            case 46:
                theFile = 'g';
                return theFile;
            case 47:
                theFile = 'h';
                return theFile;

//            Rank 7
            case 48:
                theFile = 'a';
                return theFile;
            case 49:
                theFile = 'b';
                return theFile;
            case 50:
                theFile = 'c';
                return theFile;
            case 51:
                theFile = 'd';
                return theFile;
            case 52:
                theFile = 'e';
                return theFile;
            case 53:
                theFile = 'f';
                return theFile;
            case 54:
                theFile = 'g';
                return theFile;
            case 55:
                theFile = 'h';
                return theFile;

//            Rank 8
            case 56:
                theFile = 'a';
                return theFile;
            case 57:
                theFile = 'b';
                return theFile;
            case 58:
                theFile = 'c';
                return theFile;
            case 59:
                theFile = 'd';
                return theFile;
            case 60:
                theFile = 'e';
                return theFile;
            case 61:
                theFile = 'f';
                return theFile;
            case 62:
                theFile = 'g';
                return theFile;
            case 63:
                theFile = 'h';
                return theFile;


        }
        return theFile;
    }

    //    returns current position
    protected int currentPos(char cFile, int cRank) {

        int cPos = 64;  //64 out of bounds

        switch (cFile) {
            case 'a':
                switch (cRank) {
                    case 1:
                        cPos = 0;
                        return cPos;

                    case 2:
                        cPos = 8;
                        return cPos;

                    case 3:
                        cPos = 16;
                        return cPos;

                    case 4:
                        cPos = 24;
                        return cPos;

                    case 5:
                        cPos = 32;
                        return cPos;

                    case 6:
                        cPos = 40;
                        return cPos;

                    case 7:
                        cPos = 48;
                        return cPos;

                    case 8:
                        cPos = 56;
                        return cPos;
                }

                break;

            case 'b':
                switch (cRank) {
                    case 1:
                        cPos = 1;
                        return cPos;

                    case 2:
                        cPos = 9;
                        return cPos;

                    case 3:
                        cPos = 17;
                        return cPos;

                    case 4:
                        cPos = 25;
                        return cPos;

                    case 5:
                        cPos = 33;
                        return cPos;

                    case 6:
                        cPos = 41;
                        return cPos;

                    case 7:
                        cPos = 49;
                        return cPos;

                    case 8:
                        cPos = 57;
                        return cPos;

                }
                break;

            case 'c':
                switch (cRank) {
                    case 1:
                        cPos = 2;
                        return cPos;

                    case 2:
                        cPos = 10;
                        return cPos;

                    case 3:
                        cPos = 18;
                        return cPos;

                    case 4:
                        cPos = 26;
                        return cPos;

                    case 5:
                        cPos = 34;
                        return cPos;

                    case 6:
                        cPos = 42;
                        return cPos;

                    case 7:
                        cPos = 50;
                        return cPos;

                    case 8:
                        cPos = 58;
                        return cPos;

                }
                break;

            case 'd':
                switch (cRank) {
                    case 1:
                        cPos = 3;
                        return cPos;

                    case 2:
                        cPos = 11;
                        return cPos;

                    case 3:
                        cPos = 19;
                        return cPos;

                    case 4:
                        cPos = 27;
                        return cPos;

                    case 5:
                        cPos = 35;
                        return cPos;

                    case 6:
                        cPos = 43;
                        return cPos;

                    case 7:
                        cPos = 51;
                        return cPos;

                    case 8:
                        cPos = 59;
                        return cPos;

                }
                break;

            case 'e':
                switch (cRank) {
                    case 1:
                        cPos = 4;
                        return cPos;

                    case 2:
                        cPos = 12;
                        return cPos;

                    case 3:
                        cPos = 20;
                        return cPos;

                    case 4:
                        cPos = 28;
                        return cPos;

                    case 5:
                        cPos = 36;
                        return cPos;

                    case 6:
                        cPos = 44;
                        return cPos;

                    case 7:
                        cPos = 52;
                        return cPos;

                    case 8:
                        cPos = 60;
                        return cPos;
                }
                break;

            case 'f':
                switch (cRank) {
                    case 1:
                        cPos = 5;
                        return cPos;

                    case 2:
                        cPos = 13;
                        return cPos;

                    case 3:
                        cPos = 21;
                        return cPos;

                    case 4:
                        cPos = 29;
                        return cPos;

                    case 5:
                        cPos = 37;
                        return cPos;

                    case 6:
                        cPos = 45;
                        return cPos;

                    case 7:
                        cPos = 53;
                        return cPos;

                    case 8:
                        cPos = 61;
                        return cPos;
                }
                break;

            case 'g':
                switch (cRank) {
                    case 1:
                        cPos = 6;
                        return cPos;

                    case 2:
                        cPos = 14;
                        return cPos;

                    case 3:
                        cPos = 22;
                        return cPos;

                    case 4:
                        cPos = 30;
                        return cPos;

                    case 5:
                        cPos = 38;
                        return cPos;

                    case 6:
                        cPos = 46;
                        return cPos;

                    case 7:
                        cPos = 54;
                        return cPos;

                    case 8:
                        cPos = 62;
                        return cPos;
                }
                break;

            case 'h':
                switch (cRank) {
                    case 1:
                        cPos = 7;
                        return cPos;

                    case 2:
                        cPos = 15;
                        return cPos;

                    case 3:
                        cPos = 23;
                        return cPos;

                    case 4:
                        cPos = 31;
                        return cPos;

                    case 5:
                        cPos = 39;
                        return cPos;

                    case 6:
                        cPos = 47;
                        return cPos;

                    case 7:
                        cPos = 55;
                        return cPos;

                    case 8:
                        cPos = 63;
                        return cPos;
                }
                break;
        }

        return cPos;
    }


    //    returns legal moves of pieces
    protected ArrayList<Integer> pawnMoves(int pos, String col) {
        if (col.equals("w")) {
            switch (pos) {
//            rank 2
                case 8:
//                    Log.d(TAG, "pawnMoves: white pawn1 case 8");
                    if (theBoard[16] == 0 && theBoard[24] == 0) {
                        p.add(16);
                        p.add(24);
                    } else if (theBoard[16] == 0) {
                        p.add(16);
                    }
                    if (theBoard[17] < 0) {
                        p.add(17);
                    }
                    return p;

                case 9:
                    if (theBoard[17] == 0 && theBoard[25] == 0) {
                        p.add(17);
                        p.add(25);
                    } else if (theBoard[17] == 0) {
                        p.add(17);
                    }
                    if (theBoard[16] < 0) {
                        p.add(16);
                    }
                    if (theBoard[18] < 0) {
                        p.add(18);
                    }
                    return p;

                case 10:
                    if (theBoard[18] == 0 && theBoard[26] == 0) {
                        p.add(18);
                        p.add(26);
                    } else if (theBoard[18] == 0) {
                        p.add(18);
                    }
                    if (theBoard[17] < 0) {
                        p.add(17);
                    }
                    if (theBoard[19] < 0) {
                        p.add(19);
                    }
                    return p;

                case 11:
                    if (theBoard[19] == 0 && theBoard[27] == 0) {
                        p.add(19);
                        p.add(27);
                    } else if (theBoard[19] == 0) {
                        p.add(19);
                    }
                    if (theBoard[18] < 0) {
                        p.add(18);
                    }
                    if (theBoard[20] < 0) {
                        p.add(20);
                    }
                    return p;

                case 12:
                    if (theBoard[20] == 0 && theBoard[28] == 0) {
                        p.add(20);
                        p.add(28);
                    } else if (theBoard[20] == 0) {
                        p.add(20);
                    }
                    if (theBoard[19] < 0) {
                        p.add(19);
                    }
                    if (theBoard[21] < 0) {
                        p.add(21);
                    }
                    return p;

                case 13:
                    if (theBoard[21] == 0 && theBoard[29] == 0) {
                        p.add(21);
                        p.add(29);
                    } else if (theBoard[21] == 0) {
                        p.add(21);
                    }
                    if (theBoard[20] < 0) {
                        p.add(20);
                    }
                    if (theBoard[22] < 0) {
                        p.add(22);
                    }
                    return p;

                case 14:
                    if (theBoard[22] == 0 && theBoard[30] == 0) {
                        p.add(22);
                        p.add(30);
                    } else if (theBoard[30] == 0) {
                        p.add(22);
                    }
                    if (theBoard[21] < 0) {
                        p.add(21);
                    }
                    if (theBoard[23] < 0) {
                        p.add(23);
                    }
                    return p;

                case 15:
                    if (theBoard[23] == 0 && theBoard[31] == 0) {
                        p.add(23);
                        p.add(31);
                    } else if (theBoard[23] == 0) {
                        p.add(23);
                    }
                    if (theBoard[22] < 0) {
                        p.add(22);
                    }

                    return p;

//            Rank 3
                case 16:
                    if (theBoard[24] == 0) {
                        p.add(24);
                    }
                    if (theBoard[25] < 0) {
                        p.add(25);
                    }
                    return p;

                case 17:
                    if (theBoard[25] == 0) {
                        p.add(25);
                    }
                    if (theBoard[23] < 0) {
                        p.add(23);
                    }
                    if (theBoard[25] < 0) {
                        p.add(25);
                    }
                    return p;

                case 18:
                    if (theBoard[26] == 0) {
                        p.add(26);
                    }
                    if (theBoard[25] < 0) {
                        p.add(25);
                    }
                    if (theBoard[27] < 0) {
                        p.add(27);
                    }
                    return p;

                case 19:
                    if (theBoard[27] == 0) {
                        p.add(27);
                    }
                    if (theBoard[26] < 0) {
                        p.add(26);
                    }
                    if (theBoard[28] < 0) {
                        p.add(28);
                    }
                    return p;

                case 20:
                    if (theBoard[28] == 0) {
                        p.add(28);
                    }
                    if (theBoard[27] < 0) {
                        p.add(27);
                    }
                    if (theBoard[29] < 0) {
                        p.add(29);
                    }
                    return p;

                case 21:
                    if (theBoard[29] == 0) {
                        p.add(29);
                    }
                    if (theBoard[28] < 0) {
                        p.add(28);
                    }
                    if (theBoard[30] < 0) {
                        p.add(30);
                    }
                    return p;

                case 22:
                    if (theBoard[30] == 0) {
                        p.add(30);
                    }
                    if (theBoard[29] < 0) {
                        p.add(29);
                    }
                    if (theBoard[31] < 0) {
                        p.add(31);
                    }
                    return p;

                case 23:
                    if (theBoard[31] == 0) {
                        p.add(31);
                    }
                    if (theBoard[30] < 0) {
                        p.add(30);
                    }

                    return p;

//            Rank 4
                case 24:
                    if (theBoard[32] == 0) {
                        p.add(32);
                    }
                    if (theBoard[33] < 0) {
                        p.add(32);
                    }
                    return p;

                case 25:
                    if (theBoard[33] == 0) {
                        p.add(33);
                    }
                    if (theBoard[32] < 0) {
                        p.add(32);
                    }
                    if (theBoard[34] < 0) {
                        p.add(34);
                    }
                    return p;

                case 26:
                    if (theBoard[34] == 0) {
                        p.add(34);
                    }
                    if (theBoard[33] < 0) {
                        p.add(33);
                    }
                    if (theBoard[35] < 0) {
                        p.add(35);
                    }
                    return p;

                case 27:
                    if (theBoard[35] == 0) {
                        p.add(35);
                    }
                    if (theBoard[34] < 0) {
                        p.add(34);
                    }
                    if (theBoard[36] < 0) {
                        p.add(36);
                    }
                    return p;

                case 28:
                    if (theBoard[36] == 0) {
                        p.add(36);
                    }
                    if (theBoard[35] < 0) {
                        p.add(35);
                    }
                    if (theBoard[37] < 0) {
                        p.add(37);
                    }
                    return p;

                case 29:
                    if (theBoard[37] == 0) {
                        p.add(37);
                    }
                    if (theBoard[36] < 0) {
                        p.add(36);
                    }
                    if (theBoard[38] < 0) {
                        p.add(38);
                    }
                    return p;

                case 30:
                    if (theBoard[38] == 0) {
                        p.add(38);
                    }
                    if (theBoard[37] < 0) {
                        p.add(37);
                    }
                    if (theBoard[39] < 0) {
                        p.add(39);
                    }
                    return p;

                case 31:
                    if (theBoard[39] == 0) {
                        p.add(39);
                    }
                    if (theBoard[38] < 0) {
                        p.add(38);
                    }
                    return p;

//            Rank 5
                case 32:
                    if (theBoard[40] == 0) {
                        p.add(40);
                    }
                    if (theBoard[41] < 0) {
                        p.add(41);
                    }
                    return p;

                case 33:
                    if (theBoard[41] == 0) {
                        p.add(41);
                    }
                    if (theBoard[40] < 0) {
                        p.add(40);
                    }
                    if (theBoard[42] < 0) {
                        p.add(42);
                    }
                    return p;

                case 34:
                    if (theBoard[42] == 0) {
                        p.add(42);
                    }
                    if (theBoard[41] < 0) {
                        p.add(41);
                    }
                    if (theBoard[43] < 0) {
                        p.add(43);
                    }
                    return p;

                case 35:
                    if (theBoard[43] == 0) {
                        p.add(43);
                    }
                    if (theBoard[42] < 0) {
                        p.add(42);
                    }
                    if (theBoard[44] < 0) {
                        p.add(44);
                    }
                    return p;

                case 36:
                    if (theBoard[44] == 0) {
                        p.add(44);
                    }
                    if (theBoard[43] < 0) {
                        p.add(43);
                    }
                    if (theBoard[45] < 0) {
                        p.add(45);
                    }
                    return p;

                case 37:
                    if (theBoard[45] == 0) {
                        p.add(45);
                    }
                    if (theBoard[44] < 0) {
                        p.add(44);
                    }
                    if (theBoard[46] < 0) {
                        p.add(46);
                    }
                    return p;

                case 38:
                    if (theBoard[46] == 0) {
                        p.add(46);
                    }
                    if (theBoard[45] < 0) {
                        p.add(45);
                    }
                    if (theBoard[47] < 0) {
                        p.add(47);
                    }
                    return p;

                case 39:
                    if (theBoard[47] == 0) {
                        p.add(47);
                    }
                    if (theBoard[46] < 0) {
                        p.add(46);
                    }
                    return p;

//            Rank 6
                case 40:
                    if (theBoard[48] == 0) {
                        p.add(48);
                    }
                    if (theBoard[49] < 0) {
                        p.add(49);
                    }
                    return p;

                case 41:
                    if (theBoard[49] == 0) {
                        p.add(49);
                    }
                    if (theBoard[48] < 0) {
                        p.add(48);
                    }
                    if (theBoard[50] < 0) {
                        p.add(50);
                    }
                    return p;

                case 42:
                    if (theBoard[50] == 0) {
                        p.add(50);
                    }
                    if (theBoard[49] < 0) {
                        p.add(49);
                    }
                    if (theBoard[51] < 0) {
                        p.add(51);
                    }
                    return p;

                case 43:
                    if (theBoard[51] == 0) {
                        p.add(51);
                    }
                    if (theBoard[50] < 0) {
                        p.add(50);
                    }
                    if (theBoard[52] < 0) {
                        p.add(52);
                    }
                    return p;

                case 44:
                    if (theBoard[52] == 0) {
                        p.add(52);
                    }
                    if (theBoard[51] < 0) {
                        p.add(51);
                    }
                    if (theBoard[53] < 0) {
                        p.add(53);
                    }
                    return p;

                case 45:
                    if (theBoard[53] == 0) {
                        p.add(53);
                    }
                    if (theBoard[52] < 0) {
                        p.add(52);
                    }
                    if (theBoard[54] < 0) {
                        p.add(54);
                    }
                    return p;

                case 46:
                    if (theBoard[54] == 0) {
                        p.add(54);
                    }
                    if (theBoard[53] < 0) {
                        p.add(53);
                    }
                    if (theBoard[55] < 0) {
                        p.add(55);
                    }
                    return p;

                case 47:
                    if (theBoard[55] == 0) {
                        p.add(55);
                    }
                    if (theBoard[54] < 0) {
                        p.add(54);
                    }
                    return p;

//            Rank 7
                case 48:
                    if (theBoard[56] == 0) {
                        p.add(56);
                    }
                    if (theBoard[57] < 0) {
                        p.add(57);
                    }
                    return p;

                case 49:
                    if (theBoard[57] == 0) {
                        p.add(57);
                    }
                    if (theBoard[56] < 0) {
                        p.add(56);
                    }
                    if (theBoard[58] < 0) {
                        p.add(58);
                    }
                    return p;

                case 50:
                    if (theBoard[58] == 0) {
                        p.add(58);
                    }
                    if (theBoard[57] < 0) {
                        p.add(57);
                    }
                    if (theBoard[59] < 0) {
                        p.add(59);
                    }
                    return p;

                case 51:
                    if (theBoard[59] == 0) {
                        p.add(59);
                    }
                    if (theBoard[58] < 0) {
                        p.add(58);
                    }
                    if (theBoard[60] < 0) {
                        p.add(60);
                    }
                    return p;

                case 52:
                    if (theBoard[60] == 0) {
                        p.add(60);
                    }
                    if (theBoard[59] < 0) {
                        p.add(59);
                    }
                    if (theBoard[61] < 0) {
                        p.add(61);
                    }
                    return p;

                case 53:
                    if (theBoard[61] == 0) {
                        p.add(61);
                    }
                    if (theBoard[60] < 0) {
                        p.add(60);
                    }
                    if (theBoard[62] < 0) {
                        p.add(62);
                    }
                    return p;

                case 54:
                    if (theBoard[62] == 0) {
                        p.add(62);
                    }
                    if (theBoard[61] < 0) {
                        p.add(61);
                    }
                    if (theBoard[63] < 0) {
                        p.add(63);
                    }
                    return p;

                case 55:
                    if (theBoard[63] == 0) {
                        p.add(63);
                    }
                    if (theBoard[62] < 0) {
                        p.add(62);
                    }
                    return p;

            }
        } else if (col.equals("b")) {

            switch (pos) {
//            Rank 2
                case 8:
                    if (theBoard[0] == 0) {
                        p.add(0);
                    }
                    if (theBoard[1] > 0) {
                        p.add(1);
                    }

                    return p;

                case 9:
                    if (theBoard[1] == 0) {
                        p.add(1);
                    }
                    if (theBoard[0] > 0) {
                        p.add(0);
                    }
                    if (theBoard[2] > 0) {
                        p.add(2);
                    }
                    return p;

                case 10:
                    if (theBoard[2] == 0) {
                        p.add(2);
                    }
                    if (theBoard[1] > 0) {
                        p.add(1);
                    }
                    if (theBoard[3] > 0) {
                        p.add(3);
                    }
                    return p;

                case 11:
                    if (theBoard[3] == 0) {
                        p.add(3);
                    }
                    if (theBoard[2] > 0) {
                        p.add(2);
                    }
                    if (theBoard[4] > 0) {
                        p.add(4);
                    }
                    return p;

                case 12:
                    if (theBoard[4] == 0) {
                        p.add(4);
                    }
                    if (theBoard[3] > 0) {
                        p.add(3);
                    }
                    if (theBoard[5] > 0) {
                        p.add(5);
                    }
                    return p;

                case 13:
                    if (theBoard[5] == 0) {
                        p.add(5);
                    }
                    if (theBoard[4] > 0) {
                        p.add(4);
                    }
                    if (theBoard[6] > 0) {
                        p.add(6);
                    }
                    return p;

                case 14:
                    if (theBoard[6] == 0) {
                        p.add(6);
                    }
                    if (theBoard[5] > 0) {
                        p.add(5);
                    }
                    if (theBoard[7] > 0) {
                        p.add(7);
                    }
                    return p;

                case 15:
                    if (theBoard[7] == 0) {
                        p.add(7);
                    }
                    if (theBoard[6] > 0) {
                        p.add(6);
                    }
                    return p;

//            Rank 3
                case 16:
                    if (theBoard[8] == 0) {
                        p.add(8);
                    }
                    if (theBoard[9] > 0) {
                        p.add(9);
                    }
                    return p;

                case 17:
                    if (theBoard[9] == 0) {
                        p.add(9);
                    }
                    if (theBoard[8] > 0) {
                        p.add(8);
                    }
                    if (theBoard[10] > 0) {
                        p.add(10);
                    }
                    return p;

                case 18:
                    if (theBoard[10] == 0) {
                        p.add(10);
                    }
                    if (theBoard[9] > 0) {
                        p.add(9);
                    }
                    if (theBoard[11] > 0) {
                        p.add(11);
                    }
                    return p;

                case 19:
                    if (theBoard[11] == 0) {
                        p.add(11);
                    }
                    if (theBoard[10] > 0) {
                        p.add(10);
                    }
                    if (theBoard[12] > 0) {
                        p.add(12);
                    }
                    return p;

                case 20:
                    if (theBoard[12] == 0) {
                        p.add(12);
                    }
                    if (theBoard[11] > 0) {
                        p.add(11);
                    }
                    if (theBoard[13] > 0) {
                        p.add(13);
                    }
                    return p;

                case 21:
                    if (theBoard[13] == 0) {
                        p.add(13);
                    }
                    if (theBoard[12] > 0) {
                        p.add(12);
                    }
                    if (theBoard[14] > 0) {
                        p.add(14);
                    }
                    return p;

                case 22:
                    if (theBoard[14] == 0) {
                        p.add(14);
                    }
                    if (theBoard[13] > 0) {
                        p.add(13);
                    }
                    if (theBoard[15] > 0) {
                        p.add(15);
                    }
                    return p;

                case 23:
                    Log.i(TAG, "pawnMoves: 23 ");
                    if (theBoard[15] == 0) {
                        p.add(15);
                    }
                    if (theBoard[14] > 0) {
                        p.add(14);
                    }
                    return p;

//            Rank 4
                case 24:
                    if (theBoard[16] == 0) {
                        p.add(16);
                    }
                    if (theBoard[17] > 0) {
                        p.add(17);
                    }
                    return p;

                case 25:
                    if (theBoard[17] == 0) {
                        p.add(17);
                    }
                    if (theBoard[16] > 0) {
                        p.add(16);
                    }
                    if (theBoard[18] > 0) {
                        p.add(8);
                    }
                    return p;

                case 26:
                    if (theBoard[18] == 0) {
                        p.add(18);
                    }
                    if (theBoard[17] > 0) {
                        p.add(17);
                    }
                    if (theBoard[19] > 0) {
                        p.add(19);
                    }
                    return p;

                case 27:
                    if (theBoard[19] == 0) {
                        p.add(19);
                    }
                    if (theBoard[18] > 0) {
                        p.add(18);
                    }
                    if (theBoard[20] > 0) {
                        p.add(20);
                    }
                    return p;

                case 28:
                    if (theBoard[20] == 0) {
                        p.add(20);
                    }
                    if (theBoard[19] > 0) {
                        p.add(19);
                    }
                    if (theBoard[21] > 0) {
                        p.add(21);
                    }
                    return p;

                case 29:
                    if (theBoard[21] == 0) {
                        p.add(21);
                    }
                    if (theBoard[20] > 0) {
                        p.add(20);
                    }
                    if (theBoard[22] > 0) {
                        p.add(22);
                    }
                    return p;

                case 30:
                    if (theBoard[22] == 0) {
                        p.add(22);
                    }
                    if (theBoard[21] > 0) {
                        p.add(21);
                    }
                    if (theBoard[23] > 0) {
                        p.add(23);
                    }
                    return p;

                case 31:
                    if (theBoard[23] == 0) {
                        p.add(23);
                    }
                    if (theBoard[22] > 0) {
                        p.add(22);
                    }

                    return p;

//            Rank 5
                case 32:
                    if (theBoard[24] == 0) {
                        p.add(24);
                    }
                    if (theBoard[25] > 0) {
                        p.add(25);
                    }
                    return p;

                case 33:
                    if (theBoard[25] == 0) {
                        p.add(25);
                    }
                    if (theBoard[24] > 0) {
                        p.add(24);
                    }
                    if (theBoard[26] > 0) {
                        p.add(26);
                    }
                    return p;

                case 34:
                    if (theBoard[26] == 0) {
                        p.add(26);
                    }
                    if (theBoard[25] > 0) {
                        p.add(25);
                    }
                    if (theBoard[27] > 0) {
                        p.add(27);
                    }
                    return p;

                case 35:
                    if (theBoard[27] == 0) {
                        p.add(27);
                    }
                    if (theBoard[26] > 0) {
                        p.add(26);
                    }
                    if (theBoard[28] > 0) {
                        p.add(28);
                    }
                    return p;

                case 36:
                    if (theBoard[28] == 0) {
                        p.add(28);
                    }
                    if (theBoard[27] > 0) {
                        p.add(27);
                    }
                    if (theBoard[29] > 0) {
                        p.add(29);
                    }
                    return p;

                case 37:
                    if (theBoard[29] == 0) {
                        p.add(29);
                    }
                    if (theBoard[28] > 0) {
                        p.add(28);
                    }
                    if (theBoard[30] > 0) {
                        p.add(30);
                    }
                    return p;

                case 38:
                    if (theBoard[30] == 0) {
                        p.add(30);
                    }
                    if (theBoard[29] > 0) {
                        p.add(29);
                    }
                    if (theBoard[31] > 0) {
                        p.add(31);
                    }
                    return p;

                case 39:
                    if (theBoard[31] == 0) {
                        p.add(31);
                    }
                    if (theBoard[30] > 0) {
                        p.add(30);
                    }
                    return p;

//            Rank 6
                case 40:
                    if (theBoard[32] == 0) {
                        p.add(32);
                    }
                    if (theBoard[33] > 0) {
                        p.add(33);
                    }
                    return p;

                case 41:
                    if (theBoard[33] == 0) {
                        p.add(33);
                    }
                    if (theBoard[32] > 0) {
                        p.add(32);
                    }
                    if (theBoard[34] > 0) {
                        p.add(34);
                    }
                    return p;

                case 42:
                    if (theBoard[34] == 0) {
                        p.add(34);
                    }
                    if (theBoard[33] > 0) {
                        p.add(33);
                    }
                    if (theBoard[35] > 0) {
                        p.add(35);
                    }
                    return p;

                case 43:
                    if (theBoard[35] == 0) {
                        p.add(35);
                    }
                    if (theBoard[34] > 0) {
                        p.add(34);
                    }
                    if (theBoard[36] > 0) {
                        p.add(36);
                    }
                    return p;

                case 44:
                    if (theBoard[36] == 0) {
                        p.add(36);
                    }
                    if (theBoard[35] > 0) {
                        p.add(35);
                    }
                    if (theBoard[37] > 0) {
                        p.add(37);
                    }
                    return p;

                case 45:
                    if (theBoard[37] == 0) {
                        p.add(37);
                    }
                    if (theBoard[36] > 0) {
                        p.add(36);
                    }
                    if (theBoard[38] > 0) {
                        p.add(38);
                    }
                    return p;

                case 46:
                    if (theBoard[38] == 0) {
                        p.add(38);
                    }
                    if (theBoard[36] > 0) {
                        p.add(36);
                    }
                    if (theBoard[38] > 0) {
                        p.add(38);
                    }
                    return p;

                case 47:
                    if (theBoard[39] == 0) {
                        p.add(39);
                    }
                    if (theBoard[38] > 0) {
                        p.add(38);
                    }
                    return p;

//            Rank 7
                case 48:
                    if (theBoard[40] == 0 && theBoard[32] == 0) {
                        p.add(40);
                        p.add(32);
                    } else if (theBoard[40] == 0) {
                        p.add(40);
                    }
                    if (theBoard[41] > 0) {
                        p.add(41);
                    }
                    return p;

                case 49:
                    if (theBoard[41] == 0 && theBoard[33] == 0) {
                        p.add(41);
                        p.add(33);
                    } else if (theBoard[41] == 0) {
                        p.add(41);
                    }
                    if (theBoard[40] > 0) {
                        p.add(40);
                    }
                    if (theBoard[42] > 0) {
                        p.add(42);
                    }
                    return p;

                case 50:
                    if (theBoard[42] == 0 && theBoard[34] == 0) {
                        p.add(42);
                        p.add(34);
                    } else if (theBoard[42] == 0) {
                        p.add(42);
                    }
                    if (theBoard[41] > 0) {
                        p.add(41);
                    }
                    if (theBoard[43] > 0) {
                        p.add(43);
                    }
                    return p;

                case 51:
                    if (theBoard[43] == 0 && theBoard[35] == 0) {
                        p.add(43);
                        p.add(35);
                    } else if (theBoard[43] == 0) {
                        p.add(43);
                    }
                    if (theBoard[42] > 0) {
                        p.add(42);
                    }
                    if (theBoard[44] > 0) {
                        p.add(44);
                    }
                    return p;

                case 52:
                    if (theBoard[44] == 0 && theBoard[36] == 0) {
                        p.add(44);
                        p.add(36);
                    } else if (theBoard[44] == 0) {
                        p.add(44);
                    }
                    if (theBoard[43] > 0) {
                        p.add(43);
                    }
                    if (theBoard[45] > 0) {
                        p.add(45);
                    }
                    return p;

                case 53:
                    if (theBoard[45] == 0 && theBoard[37] == 0) {
                        p.add(45);
                        p.add(37);
                    } else if (theBoard[45] == 0) {
                        p.add(45);
                    }
                    if (theBoard[44] > 0) {
                        p.add(44);
                    }
                    if (theBoard[46] > 0) {
                        p.add(46);
                    }
                    return p;

                case 54:
                    if (theBoard[46] == 0 && theBoard[38] == 0) {
                        p.add(46);
                        p.add(38);
                    } else if (theBoard[46] == 0) {
                        p.add(46);
                    }
                    if (theBoard[45] > 0) {
                        p.add(45);
                    }
                    if (theBoard[47] > 0) {
                        p.add(47);
                    }
                    return p;

                case 55:
                    if (theBoard[47] == 0 && theBoard[39] == 0) {
                        p.add(47);
                        p.add(39);
                    } else if (theBoard[47] == 0) {
                        p.add(47);
                    }
                    if (theBoard[46] > 0) {
                        p.add(46);
                    }
                    return p;
            }
        }
        return null;
    }

    protected ArrayList<Integer> wKnightMoves(int pos, String piece) {

//        is move legal
        boolean bUR = true; //up right
        boolean bUL = true; //up left
        boolean bDL = true; //down left
        boolean bDR = true; //down right
        boolean bLU = true; //left up
        boolean bLD = true; //left down
        boolean bRU = true; //right up
        boolean bRD = true; //right down

        //        y axis movement
        Integer uL = 15;    //up left
        Integer uR = 17;    //up right
        Integer dL = -17;   //down left
        Integer dR = -15;   //down right

        //        x axis movement
        Integer lU = 6; //left up
        Integer lD = -10;   //left down
        Integer rU = 10;    //right up
        Integer rD = -6;    //right down

        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        Integer posUL = pos + uL;   //sets position for determining new position
        Integer posDL = pos + dL;
        Integer posLD = pos + lD;
        Integer posLU = pos + lU;
        Integer posRU = pos + rU;
        Integer posRD = pos + rD;
        Integer posUR = pos + uR;
        Integer posDR = pos + dR;

        switch (myRank) {
            case 1:             //moving down left/right | left/right down illegal moves
                bDL = false;
                bDR = false;
                bLD = false;
                bRD = false;
                break;

            case 2:             //moving down left/right illegal moves
                bDL = false;
                bDR = false;
                break;

            case 7:             //moving up left/right illegal moves
                bUL = false;
                bUR = false;
                break;
            case 8:             //moving up left/right | left/right up illegal moves
                bUL = false;
                bUR = false;
                bLU = false;
                bRU = false;
                break;

            default:

                break;
        }

        switch (myFile) {
            case 'a':           //moving left up/down illegal moves
                bLU = false;
                bLD = false;
                bUL = false;
                bDL = false;
                break;

            case 'b':           //repeated
                bLU = false;
                bLD = false;
                break;

            case 'g':           //moving right up/down illegal moves
                bRU = false;
                bRD = false;
                break;

            case 'h':           //repeated
                bRU = false;
                bRD = false;
                bDR = false;
                bUR = false;
                break;

            default:
                break;
        }

        //up left 1
        if (bUL) {
            if (posUL >= 0 && posUL < 64) {
                if (theBoard[posUL] <= 0) {
                    switch (piece) {
                        case "wkr":
                            wKR.add(posUL);
                            break;

                        case "wkl":
                            wKL.add(posUL);
                            break;
                    }
                }
            }
        }

        //down left 2
        if (bDL) {
            if (posDL >= 0 && posDL < 64) {
                if (theBoard[posDL] <= 0) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posDL);
                            break;

                        case "wkr":
                            wKR.add(posDL);
                            break;
                    }
                }
            }
        }

        //left down 3
        if (bLD) {
            if (posLD >= 0 && posLD < 64) {
                if (theBoard[posLD] <= 0 && theBoard[posLD] > -10) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posLD);
                            break;

                        case "wkr":
                            wKR.add(posLD);
                            break;
                    }
                }
            }
        }

        //left up 4
        if (bLU) {
            if (posLU >= 0 && posLU < 64) {
                if (theBoard[posLU] <= 0) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posLU);
                            break;

                        case "wkr":
                            wKR.add(posLU);
                            break;
                    }
                }
            }
        }

        //right up 5
        if (bRU) {
            if (posRU >= 0 && posRU < 64) {
                if (theBoard[posRU] <= 0) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posRU);
                            break;

                        case "wkr":
                            wKR.add(posRU);
                            break;
                    }
                }
            }
        }

        //right down 6
        if (bRD) {
            if (posRD >= 0 && posRD < 64) {
                if (theBoard[posRD] <= 0) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posRD);
                            break;

                        case "wkr":
                            wKR.add(posRD);
                            break;
                    }
                }
            }
        }

        //up right 7
        if (bUR) {
            if (posUR >= 0 && posUR < 64) {
                if (theBoard[posUR] <= 0) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posUR);
                            break;

                        case "wkr":
                            wKR.add(posUR);
                            break;
                    }
                }
            }
        }

        //down right 8
        if (bDR) {
            if (posDR >= 0 && posDR < 64) {
                if (theBoard[posDR] <= 0) {
                    switch (piece) {
                        case "wkl":
                            wKL.add(posDR);
                            break;

                        case "wkr":
                            wKR.add(posDR);
                            break;
                    }
                }
            }
        }

        switch (piece) {
            case "wkl":
                return wKL;

            case "wkr":
                return wKR;

        }

        return null;
    }

    protected ArrayList<Integer> bKnightMoves(int pos, String piece) {


//        is move legal
        boolean bUR = true; //up right
        boolean bUL = true; //up left
        boolean bDL = true; //down left
        boolean bDR = true; //down right
        boolean bLU = true; //left up
        boolean bLD = true; //left down
        boolean bRU = true; //right up
        boolean bRD = true; //right down

        //        y axis movement
        Integer uL = 15;    //up left
        Integer uR = 17;    //up right
        Integer dL = -17;   //down left
        Integer dR = -15;   //down right

        //        x axis movement
        Integer lU = 6; //left up
        Integer lD = -10;   //left down
        Integer rU = 10;    //right up
        Integer rD = -6;    //right down

        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        Integer posUL = pos + uL;   //sets position for determining new position
        Integer posDL = pos + dL;
        Integer posLD = pos + lD;
        Integer posLU = pos + lU;
        Integer posRU = pos + rU;
        Integer posRD = pos + rD;
        Integer posUR = pos + uR;
        Integer posDR = pos + dR;

        switch (myRank) {
            case 1:             //moving down left/right | left/right down illegal moves
                bDL = false;
                bDR = false;
                bLD = false;
                bRD = false;
                break;

            case 2:             //moving down left/right illegal moves
                bDL = false;
                bDR = false;
                break;

            case 7:             //moving up left/right illegal moves
                bUL = false;
                bUR = false;
                break;
            case 8:             //moving up left/right | left/right up illegal moves
                bUL = false;
                bUR = false;
                bLU = false;
                bRU = false;
                break;

            default:

                break;
        }

        switch (myFile) {
            case 'a':           //moving left up/down illegal moves
                bLU = false;
                bLD = false;
                bUL = false;
                bDL = false;
                break;

            case 'b':           //repeated
                bLU = false;
                bLD = false;
                break;

            case 'g':           //moving right up/down illegal moves
                bRU = false;
                bRD = false;
                break;

            case 'h':           //repeated
                bRU = false;
                bRD = false;
                bDR = false;
                bUR = false;
                break;

            default:
                break;
        }

        //up left 1
        if (bUL) {
            if (posUL >= 0 && posUL < 64) {
                if (theBoard[posUL] >= 0) {
                    switch (piece) {
                        case "bkr":
                            bKR.add(posUL);
                            break;

                        case "bkl":
                            bKL.add(posUL);
                            break;
                    }
                }
            }
        }

        //down left 2
        if (bDL) {
            if (posDL >= 0 && posDL < 64) {
                if (theBoard[posDL] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posDL);
                            break;

                        case "bkr":
                            bKR.add(posDL);
                            break;

                    }
                }
            }
        }

        //left down 3
        if (bLD) {
            if (posLD >= 0 && posLD < 64) {
                if (theBoard[posLD] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posLD);
                            break;

                        case "bkr":
                            bKR.add(posLD);
                            break;
                    }
                }
            }
        }

        //left up 4
        if (bLU) {
            if (posLU >= 0 && posLU < 64) {
                if (theBoard[posLU] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posLU);
                            break;

                        case "bkr":
                            bKR.add(posLU);
                            break;

                    }
                }
            }
        }

        //right up 5
        if (bRU) {
            if (posRU >= 0 && posRU < 64) {
                if (theBoard[posRU] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posRU);
                            break;

                        case "bkr":
                            bKR.add(posRU);
                            break;

                    }
                }
            }
        }

        //right down 6
        if (bRD) {
            if (posRD >= 0 && posRD < 64) {
                if (theBoard[posRD] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posRD);
                            break;

                        case "bkr":
                            bKR.add(posRD);
                            break;
                    }
                }
            }
        }

        //up right 7
        if (bUR) {
            if (posUR >= 0 && posUR < 64) {
                if (theBoard[posUR] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posUR);
                            break;

                        case "bkr":
                            bKR.add(posUR);
                            break;

                    }
                }
            }
        }

        //down right 8
        if (bDR) {
            if (posDR >= 0 && posDR < 64) {
                if (theBoard[posDR] >= 0) {
                    switch (piece) {
                        case "bkl":
                            bKL.add(posDR);
                            break;

                        case "bkr":
                            bKR.add(posDR);
                            break;
                    }
                }

            }
        }


        switch (piece) {
            case "bkl":
                return bKL;

            case "bkr":
                return bKR;
        }

        return null;
    }

    protected ArrayList<Integer> wBishopMoves(int pos, String piece) {

//        axis movement
        Integer pl = 7;
        Integer pr = 9;
        Integer dl = -9;
        Integer dr = -7;

//        promote/demote ranges'
        int plR = 0;    //promote left range (up left)
        int dlR = 0;    //demote left range (down left)
        int prR = 0;    //promote right range (up right)
        int drR = 0;    //demote right range (down right)

        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        switch (myFile) {
            case 'a':
                switch (myRank) {
                    case 1:
                        plR = 0;
                        dlR = 0;

                        prR = 7;
                        drR = 0;
                        break;

                    case 2:
                        plR = 0;
                        dlR = 0;

                        prR = 6;
                        drR = 1;
                        break;

                    case 3:
                        plR = 0;
                        dlR = 0;

                        prR = 5;
                        drR = 2;
                        break;

                    case 4:
                        plR = 0;
                        dlR = 0;

                        prR = 4;
                        drR = 3;
                        break;


                    case 5:
                        plR = 0;
                        dlR = 0;

                        prR = 3;
                        drR = 4;
                        break;


                    case 6:
                        plR = 0;
                        dlR = 0;

                        prR = 2;
                        drR = 5;
                        break;

                    case 7:
                        plR = 0;
                        dlR = 0;

                        prR = 1;
                        drR = 6;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 0;

                        prR = 0;
                        drR = 7;
                        break;
                }
                break;

            case 'b':
                switch (myRank) {
                    case 1:
                        plR = 1;
                        dlR = 0;

                        prR = 6;
                        drR = 0;
                        break;

                    case 2:
                        plR = 1;
                        dlR = 1;

                        prR = 6;
                        drR = 1;
                        break;

                    case 3:
                        plR = 1;
                        dlR = 1;

                        prR = 5;
                        drR = 2;
                        break;

                    case 4:
                        plR = 1;
                        dlR = 1;

                        prR = 4;
                        drR = 3;
                        break;

                    case 5:
                        plR = 1;
                        dlR = 1;

                        prR = 3;
                        drR = 4;
                        break;

                    case 6:
                        plR = 1;
                        dlR = 1;

                        prR = 2;
                        drR = 5;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 1;

                        prR = 1;
                        drR = 6;
                        break;

                    case 8:
                        plR = 1;
                        dlR = 1;

                        prR = 0;
                        drR = 6;
                        break;

                }
                break;

            case 'c':
                switch (myRank) {
                    case 1:
                        plR = 2;
                        dlR = 0;

                        prR = 5;
                        drR = 0;
                        break;

                    case 2:
                        plR = 2;
                        dlR = 1;

                        prR = 5;
                        drR = 1;
                        break;

                    case 3:
                        plR = 2;
                        dlR = 2;

                        prR = 5;
                        drR = 2;
                        break;

                    case 4:
                        plR = 2;
                        dlR = 2;

                        prR = 4;
                        drR = 3;
                        break;

                    case 5:
                        plR = 2;
                        dlR = 2;

                        prR = 3;
                        drR = 4;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 2;

                        prR = 2;
                        drR = 5;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 2;

                        prR = 1;
                        drR = 5;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 2;

                        prR = 0;
                        drR = 5;
                        break;

                }
                break;

            case 'd':
                switch (myRank) {
                    case 1:
                        plR = 3;
                        dlR = 0;

                        prR = 4;
                        drR = 0;
                        break;

                    case 2:
                        plR = 3;
                        dlR = 1;

                        prR = 4;
                        drR = 1;
                        break;

                    case 3:
                        plR = 3;
                        dlR = 2;

                        prR = 4;
                        drR = 2;
                        break;

                    case 4:
                        plR = 3;
                        dlR = 3;

                        prR = 4;
                        drR = 3;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 3;

                        prR = 3;
                        drR = 4;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 3;

                        prR = 2;
                        drR = 4;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 3;

                        prR = 1;
                        drR = 4;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 3;

                        prR = 0;
                        drR = 4;
                        break;

                }
                break;

            case 'e':
                switch (myRank) {
                    case 1:
                        plR = 4;
                        dlR = 0;

                        prR = 3;
                        drR = 0;
                        break;

                    case 2:
                        plR = 4;
                        dlR = 1;

                        prR = 3;
                        drR = 1;
                        break;

                    case 3:
                        plR = 4;
                        dlR = 2;

                        prR = 3;
                        drR = 2;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 3;
                        drR = 3;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 3;
                        drR = 3;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 4;

                        prR = 2;
                        drR = 3;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 4;

                        prR = 1;
                        drR = 3;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 4;

                        prR = 0;
                        drR = 3;
                        break;

                }
                break;

            case 'f':
                switch (myRank) {
                    case 1:
                        plR = 5;
                        dlR = 0;

                        prR = 2;
                        drR = 0;
                        break;

                    case 2:
                        plR = 5;
                        dlR = 1;

                        prR = 0;
                        drR = 1;
                        break;

                    case 3:
                        plR = 5;
                        dlR = 0;

                        prR = 2;
                        drR = 2;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 2;
                        drR = 2;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 2;
                        drR = 2;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 5;

                        prR = 2;
                        drR = 2;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 6;

                        prR = 2;
                        drR = 2;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 7;

                        prR = 0;
                        drR = 2;
                        break;

                }
                break;

            case 'g':
                switch (myRank) {
                    case 1:
                        plR = 6;
                        dlR = 0;

                        prR = 1;
                        drR = 0;
                        break;

                    case 2:
                        plR = 6;
                        dlR = 1;

                        prR = 1;
                        drR = 1;
                        break;

                    case 3:
                        plR = 5;
                        dlR = 2;

                        prR = 1;
                        drR = 1;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 1;
                        drR = 1;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 1;
                        drR = 1;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 5;

                        prR = 1;
                        drR = 1;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 6;

                        prR = 1;
                        drR = 1;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 5;

                        prR = 1;
                        drR = 1;
                        break;

                }
                break;

            case 'h':
                switch (myRank) {
                    case 1:
                        plR = 7;
                        dlR = 0;

                        prR = 0;
                        drR = 0;
                        break;

                    case 2:
                        plR = 6;
                        dlR = 1;

                        prR = 0;
                        drR = 0;
                        break;

                    case 3:
                        plR = 5;
                        dlR = 2;

                        prR = 0;
                        drR = 0;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 0;
                        drR = 0;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 0;
                        drR = 0;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 5;

                        prR = 0;
                        drR = 0;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 6;

                        prR = 0;
                        drR = 0;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 7;

                        prR = 0;
                        drR = 0;
                        break;
                }
                break;
        }

        Integer tempPos = pos;

//        promote left
        for (int i = 1; i <= plR; i++) {
            tempPos += i * (pl);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote left range
        for (int i = 1; i <= dlR; i++) {
            tempPos += i * (dl);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //promote right range
        for (int i = 1; i <= prR; i++) {
            tempPos += i * (pr);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote right range
        for (int i = 1; i <= drR; i++) {
            tempPos += i * (dr);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wBL.add(tempPos);
                    wBR.add(tempPos);
                    break;
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        switch (piece) {
            case "wbl":
                return wBL;

            case "wbr":
                return wBR;
        }
        return null;
    }

    protected ArrayList<Integer> bBishopMoves(int pos, String piece) {

//        axis movement
        Integer pl = 7;
        Integer pr = 9;
        Integer dl = -9;
        Integer dr = -7;

//        promote/demote ranges'
        int plR = 0;    //promote left range (up left)
        int dlR = 0;    //demote left range (down left)
        int prR = 0;    //promote right range (up right)
        int drR = 0;    //demote right range (down right)

        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        switch (myFile) {
            case 'a':
                switch (myRank) {
                    case 1:
                        plR = 0;
                        dlR = 0;

                        prR = 7;
                        drR = 0;
                        break;

                    case 2:
                        plR = 0;
                        dlR = 0;

                        prR = 6;
                        drR = 1;
                        break;

                    case 3:
                        plR = 0;
                        dlR = 0;

                        prR = 5;
                        drR = 2;
                        break;

                    case 4:
                        plR = 0;
                        dlR = 0;

                        prR = 4;
                        drR = 3;
                        break;


                    case 5:
                        plR = 0;
                        dlR = 0;

                        prR = 3;
                        drR = 4;
                        break;


                    case 6:
                        plR = 0;
                        dlR = 0;

                        prR = 2;
                        drR = 5;
                        break;

                    case 7:
                        plR = 0;
                        dlR = 0;

                        prR = 1;
                        drR = 6;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 0;

                        prR = 0;
                        drR = 7;
                        break;
                }
                break;

            case 'b':
                switch (myRank) {
                    case 1:
                        plR = 1;
                        dlR = 0;

                        prR = 6;
                        drR = 0;
                        break;

                    case 2:
                        plR = 1;
                        dlR = 1;

                        prR = 6;
                        drR = 1;
                        break;

                    case 3:
                        plR = 1;
                        dlR = 1;

                        prR = 5;
                        drR = 2;
                        break;

                    case 4:
                        plR = 1;
                        dlR = 1;

                        prR = 4;
                        drR = 3;
                        break;

                    case 5:
                        plR = 1;
                        dlR = 1;

                        prR = 3;
                        drR = 4;
                        break;

                    case 6:
                        plR = 1;
                        dlR = 1;

                        prR = 2;
                        drR = 5;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 1;

                        prR = 1;
                        drR = 6;
                        break;

                    case 8:
                        plR = 1;
                        dlR = 1;

                        prR = 0;
                        drR = 6;
                        break;

                }
                break;

            case 'c':
                switch (myRank) {
                    case 1:
                        plR = 2;
                        dlR = 0;

                        prR = 5;
                        drR = 0;
                        break;

                    case 2:
                        plR = 2;
                        dlR = 1;

                        prR = 5;
                        drR = 1;
                        break;

                    case 3:
                        plR = 2;
                        dlR = 2;

                        prR = 5;
                        drR = 2;
                        break;

                    case 4:
                        plR = 2;
                        dlR = 2;

                        prR = 4;
                        drR = 3;
                        break;

                    case 5:
                        plR = 2;
                        dlR = 2;

                        prR = 3;
                        drR = 4;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 2;

                        prR = 2;
                        drR = 5;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 2;

                        prR = 1;
                        drR = 5;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 2;

                        prR = 0;
                        drR = 5;
                        break;

                }
                break;

            case 'd':
                switch (myRank) {
                    case 1:
                        plR = 3;
                        dlR = 0;

                        prR = 4;
                        drR = 0;
                        break;

                    case 2:
                        plR = 3;
                        dlR = 1;

                        prR = 4;
                        drR = 1;
                        break;

                    case 3:
                        plR = 3;
                        dlR = 2;

                        prR = 4;
                        drR = 2;
                        break;

                    case 4:
                        plR = 3;
                        dlR = 3;

                        prR = 4;
                        drR = 3;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 3;

                        prR = 3;
                        drR = 4;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 3;

                        prR = 2;
                        drR = 4;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 3;

                        prR = 1;
                        drR = 4;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 3;

                        prR = 0;
                        drR = 4;
                        break;

                }
                break;

            case 'e':
                switch (myRank) {
                    case 1:
                        plR = 4;
                        dlR = 0;

                        prR = 3;
                        drR = 0;
                        break;

                    case 2:
                        plR = 4;
                        dlR = 1;

                        prR = 3;
                        drR = 1;
                        break;

                    case 3:
                        plR = 4;
                        dlR = 2;

                        prR = 3;
                        drR = 2;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 3;
                        drR = 3;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 3;
                        drR = 3;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 4;

                        prR = 2;
                        drR = 3;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 4;

                        prR = 1;
                        drR = 3;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 4;

                        prR = 0;
                        drR = 3;
                        break;

                }
                break;

            case 'f':
                switch (myRank) {
                    case 1:
                        plR = 5;
                        dlR = 0;

                        prR = 2;
                        drR = 0;
                        break;

                    case 2:
                        plR = 5;
                        dlR = 1;

                        prR = 0;
                        drR = 1;
                        break;

                    case 3:
                        plR = 5;
                        dlR = 0;

                        prR = 2;
                        drR = 2;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 2;
                        drR = 2;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 2;
                        drR = 2;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 5;

                        prR = 2;
                        drR = 2;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 6;

                        prR = 2;
                        drR = 2;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 5;

                        prR = 0;
                        drR = 2;
                        break;

                }
                break;

            case 'g':
                switch (myRank) {
                    case 1:
                        plR = 6;
                        dlR = 0;

                        prR = 1;
                        drR = 0;
                        break;

                    case 2:
                        plR = 6;
                        dlR = 1;

                        prR = 1;
                        drR = 1;
                        break;

                    case 3:
                        plR = 5;
                        dlR = 2;

                        prR = 1;
                        drR = 1;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 1;
                        drR = 1;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 1;
                        drR = 1;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 5;

                        prR = 1;
                        drR = 1;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 6;

                        prR = 1;
                        drR = 1;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 5;

                        prR = 1;
                        drR = 1;
                        break;

                }
                break;

            case 'h':
                switch (myRank) {
                    case 1:
                        plR = 7;
                        dlR = 0;

                        prR = 0;
                        drR = 0;
                        break;

                    case 2:
                        plR = 6;
                        dlR = 1;

                        prR = 0;
                        drR = 0;
                        break;

                    case 3:
                        plR = 5;
                        dlR = 2;

                        prR = 0;
                        drR = 0;
                        break;

                    case 4:
                        plR = 4;
                        dlR = 3;

                        prR = 0;
                        drR = 0;
                        break;

                    case 5:
                        plR = 3;
                        dlR = 4;

                        prR = 0;
                        drR = 0;
                        break;

                    case 6:
                        plR = 2;
                        dlR = 5;

                        prR = 0;
                        drR = 0;
                        break;

                    case 7:
                        plR = 1;
                        dlR = 6;

                        prR = 0;
                        drR = 0;
                        break;

                    case 8:
                        plR = 0;
                        dlR = 7;

                        prR = 0;
                        drR = 0;
                        break;
                }
                break;
        }

        Integer tempPos = pos;

//        promote left range
        for (int i = 1; i <= plR; i++) {
            tempPos += i * pl;            //e.g. range is 3, multiple 1*pl, 2*pl, 3*pl

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    tempPos = pos;
                    Log.i(TAG, "bBishopMoves: promote left test");
                } else if (theBoard[tempPos] > 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote left range
        for (int i = 1; i <= dlR; i++) {
            tempPos += i * (dl);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    tempPos = pos;
                    Log.i(TAG, "bBishopMoves: demote left test 0");
                } else if (theBoard[tempPos] > 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    Log.i(TAG, "bBishopMoves: demote left test > 1");
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //promote right range
        for (int i = 1; i <= prR; i++) {
            tempPos += i * (pr);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    tempPos = pos;
                    Log.i(TAG, "bBishopMoves: promote right");
                } else if (theBoard[tempPos] > 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote right range
        for (int i = 1; i <= drR; i++) {
            tempPos += i * (dr);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    tempPos = pos;
                    Log.i(TAG, "bBishopMoves: demote right test");
                } else if (theBoard[tempPos] > 0) {
                    bBL.add(tempPos);
                    bBR.add(tempPos);
                    break;
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        switch (piece) {
            case "bbl":
                return bBL;

            case "bbr":
                return bBR;
        }
        return null;

    }

    protected ArrayList<Integer> wRookMoves(int pos, String piece) {

//        promote Demote/right Left
        Integer p = 8;
        Integer d = -8;
        Integer r = 1;
        Integer l = -1;

//        range: up, down, left, right
        Integer pR = 0;
        Integer dR = 0;
        Integer lR = 0;
        Integer rR = 0;


        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        switch (myFile) {
            case 'a':
                lR = 0;
                rR = 7;
                break;

            case 'b':
                lR = 1;
                rR = 6;
                break;

            case 'c':
                lR = 2;
                rR = 5;
                break;

            case 'd':
                lR = 3;
                rR = 4;
                break;

            case 'e':
                lR = 4;
                rR = 3;
                break;

            case 'f':
                lR = 5;
                rR = 2;
                break;

            case 'g':
                lR = 6;
                rR = 1;
                break;

            case 'h':
                rR = 0;
                lR = 7;
                break;
        }

        switch (myRank) {
            case 1:
                dR = 0;
                pR = 7;
                break;
            case 2:
                dR = 1;
                pR = 6;
                break;
            case 3:
                dR = 2;
                pR = 5;
                break;
            case 4:
                dR = 3;
                pR = 4;
                break;
            case 5:
                dR = 4;
                pR = 3;
                break;
            case 6:
                dR = 5;
                pR = 2;
                break;
            case 7:
                dR = 6;
                pR = 1;
                break;
            case 8:
                pR = 0;
                dR = 7;
                break;
        }

        int tempPos = pos;

//        promote range
        for (int i = 0; i < pR; i++) {
            tempPos += p;

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                } else if (theBoard[tempPos] < 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }


//        //demote range
        for (int i = 1; i <= dR; i++) {
            tempPos += i * (d);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }



//        //left range
        for (int i = 1; i <= lR; i++) {
            tempPos += i * (l);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

//        // right range
        for (int i = 1; i <= rR; i++) {
            tempPos += i * (r);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wRL.add(tempPos);
                    wRR.add(tempPos);
                    break;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
//
        switch (piece) {
            case "wrl":
                return wRL;

            case "wrr":
                return wRR;

        }
        return null;
    }

    protected ArrayList<Integer> bRookMoves(int pos, String piece) {

//        promote Demote/right Left
        Integer p = 8;
        Integer d = -8;
        Integer r = 1;
        Integer l = -1;

//        range: up, down, left, right
        Integer pR = 0;
        Integer dR = 0;
        Integer lR = 0;
        Integer rR = 0;


        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        switch (myFile) {
            case 'a':
                lR = 0;
                rR = 7;
                break;

            case 'b':
                lR = 1;
                rR = 6;
                break;

            case 'c':
                lR = 2;
                rR = 5;
                break;

            case 'd':
                lR = 3;
                rR = 4;
                break;

            case 'e':
                lR = 4;
                rR = 3;
                break;

            case 'f':
                lR = 5;
                rR = 2;
                break;

            case 'g':
                lR = 6;
                rR = 1;
                break;

            case 'h':
                rR = 0;
                lR = 7;
                break;
        }

        switch (myRank) {
            case 1:
                dR = 0;
                pR = 7;
                break;
            case 2:
                dR = 1;
                pR = 6;
                break;
            case 3:
                dR = 2;
                pR = 5;
                break;
            case 4:
                dR = 3;
                pR = 4;
                break;
            case 5:
                dR = 4;
                pR = 3;
                break;
            case 6:
                dR = 5;
                pR = 2;
                break;
            case 7:
                dR = 6;
                pR = 1;
                break;
            case 8:
                pR = 0;
                dR = 7;
                break;
        }

        int tempPos = pos;

        for (int i = 0; i < pR; i++) {
            tempPos += p;

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                } else if (theBoard[tempPos] > 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }


        //        //demote range
        for (int i = 1; i <= dR; i++) {
            tempPos += i * (d);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] > 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

//        //left range
        for (int i = 1; i <= lR; i++) {
            tempPos += i * (l);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] > 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }
//
//        // right range
        for (int i = 1; i <= rR; i++) {
            tempPos += i * (r);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] > 0) {
                    bRL.add(tempPos);
                    bRR.add(tempPos);
                    break;
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        switch (piece) {
            case "brl":
                return bRL;

            case "brr":
                return bRR;

        }
        return null;
    }

    protected ArrayList<Integer> kingMoves(int pos, String piece) {

//        promote|demote/right|left
//        promote: left/right|demote:left/right
        Integer p = 8;
        Integer d = -8;
        Integer r = 1;
        Integer l = -1;

//        diagonals
        Integer pl = 7;
        Integer pr = 9;
        Integer dl = -9;
        Integer dr = -7;

        //        ranges: promote, demote, left, right / promoteLeft, demoteLeft, promoteRight, demoteRight
        boolean pR = true;
        boolean dR = true;
        boolean lR = true;
        boolean rR = true;
        boolean plR = true;    //promote left range (up left)
        boolean dlR = true;    //demote left range (down    left)
        boolean prR = true;    //promote right range (up right)
        boolean drR = true;    //demote right range (down right)

        Integer posP = pos + p;     //promote
        Integer posPL = pos + pl;  //promote left
        Integer posPR = pos + pr;   //promote right
        Integer posD = pos + d;     //demote
        Integer posDL = pos + dl;   //demote left
        Integer posDR = pos + dr;   //demote right
        Integer posL = pos + l;     //left
        Integer posR = pos + r;     //right


        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        switch (myFile) {
            case 'a':
                lR = false;
                plR = false;
                dlR = false;
                break;

            case 'h':
                rR = false;
                prR = false;
                drR = false;
                break;
        }

        switch (myRank) {
            case 1:
                dR = false;
                drR = false;
                dlR = false;
                break;
            case 8:
                pR = false;
                prR = false;
                plR = false;
                break;
        }

//        promote
        if (pR) {
            if (posP >= 0 && posP < 64) {
                if (theBoard[posP] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posP);
                            break;
                        case "bk":
                            bK.add(posP);
                            break;
                    }
                } else if (theBoard[posP] < 0) {
                    wK.add(posP);
                } else if (theBoard[posP] > 0) {
                    bK.add(posP);
                }
            }
        }

//        promote left
        if (plR) {
            if (posPL >= 0 && posPL < 64) {
                if (theBoard[posPL] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posPL);
                            break;

                        case "bk":
                            bK.add(posPL);
                            break;
                    }

                } else if (theBoard[posPL] < 0) {
                    wK.add(posPL);
                } else if (theBoard[posPL] > 0) {
                    bK.add(posPL);
                }
            }
        }

//        promote right
        if (prR) {
            if (posPR >= 0 && posPR < 64) {
                if (theBoard[posPR] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posPR);
                            break;

                        case "bk":
                            bK.add(posPR);
                            break;
                    }
                } else if (theBoard[posPR] < 0) {
                    wK.add(posPR);
                } else if (theBoard[posPR] > 0) {
                    bK.add(posPR);
                }
            }
        }

//        demote
        if (dR) {
            if (posD >= 0 && posD < 64) {
                if (theBoard[posD] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posD);
                            break;

                        case "bk":
                            bK.add(posD);
                            break;
                    }
                } else if (theBoard[posD] < 0) {
                    wK.add(posD);
                } else if (theBoard[posD] > 0) {
                    bK.add(posD);
                }
            }
        }


//        demote left
        if (dlR) {
            if (posDL >= 0 && posDL < 64) {
                if (theBoard[posDL] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posDL);
                            break;

                        case "bk":
                            bK.add(posDL);
                            break;
                    }
                } else if (theBoard[posDL] < 0) {
                    wK.add(posDL);
                } else if (theBoard[posDL] > 0) {
                    bK.add(posDL);
                }
            }
        }

//        demote right
        if (drR) {
            if (posDR >= 0 && posDR < 64) {
                if (theBoard[posDR] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posDR);
                            break;

                        case "bk":
                            bK.add(posDR);
                            break;
                    }
                } else if (theBoard[posDR] < 0) {
                    wK.add(posDR);
                } else if (theBoard[posDR] > 0) {
                    bK.add(posDR);
                }
            }
        }

//        left
        if (lR) {
            if (posL >= 0 && posL < 64) {
                if (theBoard[posL] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posL);
                            break;

                        case "bk":
                            bK.add(posL);
                            break;
                    }
                } else if (theBoard[posL] < 0) {
                    wK.add(posL);
                } else if (theBoard[posL] > 0) {
                    bK.add(posL);
                }
            }
        }

//        right
        if (rR) {
            if (posR >= 0 && posR < 64) {
                if (theBoard[posR] == 0) {
                    switch (piece) {
                        case "wk":
                            wK.add(posR);
                            break;

                        case "bk":
                            bK.add(posR);
                            break;
                    }
                } else if (theBoard[posR] < 0) {
                    wK.add(posR);
                } else if (theBoard[posR] > 0) {
                    bK.add(posR);
                }
            }
        }
        switch (piece) {
            case "wk":
                return wK;
            case "bk":
                return bK;
        }
        return null;

    }

    protected ArrayList<Integer> queenMoves(int pos, String piece) {

//        promote|demote/right|left
//        promote: left/right|demote:left/right
        Integer p = 8;
        Integer d = -8;
        Integer r = 1;
        Integer l = -1;

//        diagonals
        Integer pl = 7;
        Integer pr = 9;
        Integer dl = -9;
        Integer dr = -7;

//        ranges: promote, demote, left, right / promoteLeft, demoteLeft, promoteRight, demoteRight
        Integer pR = 0;
        Integer dR = 0;
        Integer lR = 0;
        Integer rR = 0;
        Integer plR = 0;    //promote left range (up left)
        Integer dlR = 0;    //demote left range (down left)
        Integer prR = 0;    //promote right range (up right)
        Integer drR = 0;    //demote right range (down right)


        char myFile = currentFile(pos);
        int myRank = currentRank(pos);

        switch (myFile) {
            case 'a':
                lR = 0;
                rR = 7;
                plR = 0;
                dlR = 0;
                prR = 7;
                drR = 7;
                break;

            case 'b':
                lR = 1;
                rR = 6;
                plR = 1;
                dlR = 1;
                prR = 6;
                drR = 6;
                break;

            case 'c':
                lR = 2;
                rR = 5;
                plR = 2;
                dlR = 2;
                prR = 5;
                drR = 5;
                break;

            case 'd':
                lR = 3;
                rR = 4;
                plR = 3;
                dlR = 3;
                prR = 4;
                drR = 4;
                break;

            case 'e':
                lR = 4;
                rR = 3;
                plR = 4;
                dlR = 4;
                prR = 3;
                drR = 3;
                break;

            case 'f':
                lR = 5;
                rR = 2;
                plR = 5;
                dlR = 5;
                prR = 2;
                drR = 2;
                break;

            case 'g':
                lR = 6;
                rR = 1;
                plR = 6;
                dlR = 6;
                prR = 1;
                drR = 1;
                break;

            case 'h':
                lR = 7;
                rR = 0;
                plR = 7;
                dlR = 7;
                prR = 0;
                drR = 0;
                break;
        }

        switch (myRank) {
            case 1:
                dR = 0;
                pR = 7;
                break;
            case 2:
                dR = 1;
                pR = 6;
                break;
            case 3:
                dR = 2;
                pR = 5;
                break;
            case 4:
                dR = 3;
                pR = 4;
                break;
            case 5:
                dR = 4;
                pR = 3;
                break;
            case 6:
                dR = 5;
                pR = 2;
                break;
            case 7:
                dR = 6;
                pR = 1;
                break;
            case 8:
                pR = 0;
                dR = 7;
                break;
        }

        int tempPos = pos;

//        promote left range
        for (int i = 1; i <= plR; i++) {
            tempPos += i * pl;

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote left range
        for (int i = 1; i <= dlR; i++) {
            tempPos += i * (dl);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //promote right range
        for (int i = 1; i <= prR; i++) {
            tempPos += i * (pr);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote right range
        for (int i = 1; i <= drR; i++) {
            tempPos += i * (dr);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //promote range
        for (int i = 1; i <= pR; i++) {
            tempPos += p;
//            i * (p);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
//                    tempPos = pos;
                    Log.i(TAG, "queenMoves: reeee");
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        //demote range
        for (int i = 1; i <= dR; i++) {
            tempPos += i * (d);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

//        //left range
        for (int i = 1; i <= lR; i++) {
            tempPos += i * (l);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    tempPos = pos;
                    break;
                } else {
                    tempPos = pos;
                    break;
                }
            } else {
                tempPos = pos;
                break;
            }
        }

        // right range
        for (int i = 1; i <= rR; i++) {
            tempPos += i * (r);

            if (tempPos >= 0 && tempPos < 64) {
                if (theBoard[tempPos] == 0) {
                    wQ.add(tempPos);
                    bQ.add(tempPos);
                    tempPos = pos;
                } else if (theBoard[tempPos] < 0) {
                    wQ.add(tempPos);
                    break;
                } else if (theBoard[tempPos] > 0) {
                    bQ.add(tempPos);
                    break;
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        switch (piece) {
            case "wq":
                return wQ;

            case "bq":
                return bQ;

        }
        return null;
    }

}